# Copyright (c) 2021 Prasetiyo Hadi Purwoko <prasetiyohadi92@gmail.com>.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License

ARG PYTHON_BASE_IMAGE=pras/python-base-image:latest
ARG PYTHON_BUILDER_IMAGE=pras/python-builder-image:latest

FROM $PYTHON_BUILDER_IMAGE as builder
# =============================================================================
ARG ZUUL_SIBLINGS

# build this library (meaning ansible-builder)
COPY . /tmp/src
RUN assemble
FROM $PYTHON_BASE_IMAGE
# =============================================================================

COPY --from=builder /output/ /output
# building EEs require the install-from-bindep script, but not the rest of the /output folder
RUN /output/install-from-bindep && find /output/* -not -name install-from-bindep -exec rm -rf {} +

# move the assemble scripts themselves into this container
COPY --from=builder /usr/local/bin/assemble /usr/local/bin/assemble
COPY --from=builder /usr/local/bin/get-extras-packages /usr/local/bin/get-extras-packages
