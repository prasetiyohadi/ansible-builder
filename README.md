# Ansible Builder

A container image that contains installation of [Ansible Builder](https://pypi.org/project/ansible-builder/) based on configuration in [ansible/ansible-builder](https://github.com/ansible/ansible-builder).

Ansible Builder is a tool that automates the process of building execution
environments using the schemas and tooling defined in various Ansible
Collections and by the user.


See the readthedocs page for `ansible-builder` at:

https://ansible-builder.readthedocs.io/en/latest/
